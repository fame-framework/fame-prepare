package de.dlr.gitlab.fame;

import java.io.IOException;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import com.google.auto.service.AutoService;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.Input;
import de.dlr.gitlab.fame.agent.input.ParameterTree;

/**
 * Processes FAME's {@link Input} Annotations to check their valid application and to create interfaces for {@link Agent}'s
 * {@link ParameterTree}s
 *
 * @author Christoph Schimeczek
 */
@SupportedAnnotationTypes("de.dlr.gitlab.fame.agent.input.Input")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor.class)
public class InputProcessor extends AbstractProcessor {
	public static final String ERR_WRONG_ANNOTATION = "Cannot handle annotation of type ";
	public static final String ERR_NOT_PARAM_TREE = "@Input can only be applied to type ";
	public static final String ERR_WRITE = "Could not generate source file.";
	public static final String WARN_NOT_AGENT = "Annotation ignored. Element annotated with @Input must be field of Agent class.";
	public static final String ERR_NOT_STATIC_FINAL = "@Input can only be applied to 'static final' fields.";

	private Types typeUtils;
	private Elements elementUtils;

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		setUtils();

		for (TypeElement annotation : annotations) {
			if (!canHandle(annotation)) {
				raiseError(ERR_WRONG_ANNOTATION + annotation.getQualifiedName());
				continue;
			}

			Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
			Set<VariableElement> fields = ElementFilter.fieldsIn(annotatedElements);
			for (VariableElement field : fields) {
				if (!checksPassed(field)) {
					continue;
				}
				generateInputInterface(field);
				// TODO: addToSchema
			}
		}
		return true;
	}

	/** sets utilities obtained from the processing environment */
	private void setUtils() {
		typeUtils = processingEnv.getTypeUtils();
		elementUtils = processingEnv.getElementUtils();
	}

	/** @return true if given Type can be handled */
	private boolean canHandle(TypeElement annotation) {
		return "de.dlr.gitlab.fame.agent.input.Input".contentEquals(annotation.getQualifiedName());
	}

	/** logs the given message and raises an unspecific compilation error */
	private void raiseError(String message) {
		processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, message);
	}

	/** @return true if validity checks for the given element are passed, otherwise returns false and raises compilation errors */
	private boolean checksPassed(Element annotatedElement) {
		boolean passed = true;
		if (!isParameterTree(annotatedElement)) {
			raiseError(ERR_NOT_PARAM_TREE + ParameterTree.class.getSimpleName(), annotatedElement);
			passed = false;
		} else if (!isFieldOfAgent(annotatedElement)) {
			raiseWarn(WARN_NOT_AGENT, annotatedElement);
			passed = false;
		} else if (!isStaticFinal(annotatedElement)) {
			raiseError(ERR_NOT_STATIC_FINAL, annotatedElement);
			passed = false;
		}
		return passed;
	}

	/** @return true if the given element is a {@link ParameterTree} */
	private boolean isParameterTree(Element annotatedElement) {
		TypeMirror parameterTreeMirror = elementUtils.getTypeElement(ParameterTree.class.getCanonicalName()).asType();
		return typeUtils.isAssignable(annotatedElement.asType(), parameterTreeMirror);
	}

	/** @return true if the given element is Field of an {@link Agent} */
	private boolean isFieldOfAgent(Element annotatedElement) {
		TypeMirror agentMirror = elementUtils.getTypeElement(Agent.class.getCanonicalName()).asType();
		TypeMirror enclosingType = annotatedElement.getEnclosingElement().asType();
		return typeUtils.isAssignable(enclosingType, agentMirror);
	}

	/** logs the given message and raises a compilation error associated with the given element */
	private void raiseError(String message, Element element) {
		processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, message, element);
	}

	/** logs the given message and raises a compilation warning associated with the given element */
	private void raiseWarn(String message, Element element) {
		processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, message, element);
	}

	/** @return true if the given element is defined to be static final */
	private boolean isStaticFinal(Element annotatedElement) {
		Set<Modifier> modifiers = annotatedElement.getModifiers();
		return modifiers.contains(Modifier.STATIC) && modifiers.contains(Modifier.FINAL);
	}

	/**
	 * creates source files for the annotatedElement
	 * 
	 * @throws IOException
	 */
	private void generateInputInterface(Element annotatedElement) {
		//
		// try {
		// JavaFileObject builderFile = processingEnv.getFiler().createSourceFile("");
		// }
		// catch (IOException e) {
		// raiseError(ERR_WRITE, annotatedElement);
		// }
		////
		// try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
		// out.print("dummyContent");
		// }
	}
}
