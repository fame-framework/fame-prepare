package de.dlr.gitlab.fame;

import java.util.ArrayList;
import javax.tools.JavaFileObject;
import com.google.testing.compile.JavaFileObjects;

/** Creates an artificial source file */
public class SourceFileBuilder {
	private final String className;
	private final ArrayList<String> imports = new ArrayList<>();
	private final ArrayList<String> lines = new ArrayList<>();
	private String extension;
	private String packageName;
	
	
	private SourceFileBuilder(String className) {
		this.className = className;
	}
	
	public static SourceFileBuilder newBuilder(String className) {
		return new SourceFileBuilder(className);
	}
		
	public JavaFileObject build() {
		StringBuilder stringBuilder = new StringBuilder();
		if (packageName != null && !packageName.trim().isEmpty()) {
			stringBuilder.append("package ").append(packageName).append(";\n");
		}
		imports.forEach(x -> stringBuilder.append("import ").append(x).append(";\n"));
		stringBuilder.append("\nclass ").append(className);
		if (extension != null && !extension.trim().isEmpty()) {
			stringBuilder.append(" extends ").append(extension);
		}
		stringBuilder.append(" {\n");
		lines.forEach(x -> stringBuilder.append(x).append("\n"));
		stringBuilder.append("}\n");
		
		return JavaFileObjects.forSourceString(className, stringBuilder.toString());
	}
	
	/** set an import statement */
	public SourceFileBuilder impo(String line) {
		imports.add(line);
		return this;
	}
	
	public SourceFileBuilder line(String line) {
		lines.add(line);
		return this;
	}
	
	public SourceFileBuilder pack(String packageName) {
		this.packageName = packageName;
		return this;
	}
	
	/** optionally set the class that this source extends */
	public SourceFileBuilder ext(String extension) {
		this.extension = extension;
		return this;
	}
}
